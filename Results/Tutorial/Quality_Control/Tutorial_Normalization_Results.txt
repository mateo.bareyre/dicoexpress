################################################
Filtering and Normalization
################################################

#### Description of Raw counts table ####
Number of samples: 24 
Number of genes: 33602 

#### Filtering ####
Number of genes discarded by the filtering: 14375 
Number of genes analyzed after filtering: 19227 

################################################
 Statistics on the normalization factors 
################################################

   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.9447  0.9826  1.0000  1.0004  1.0197  1.0604 

