#### Clusters Enrichment analysis
enrichment_analysis <- function(Reference,Gene_List,Alpha){
    ## success in the urn /  For each annotation term, number of annotated genes in the Reference file
    m=table(Reference[,2])
    ## failures in the urn / For each annotation term, number of not annotated genes in the Reference file
    n=length(unique(Reference[,1]))-m
    
    trial<-merge(Gene_List,Reference)
    ## trial effective / number of genes in the gene list
    k=length(unique(trial[,1]))
    ## trial success /  For each annotation term, number of annotated genes in the gene list file
    x=table(factor(trial[,2],levels=rownames(m)))
    ## Result files
    res=NaN
    Term=rownames(m)
    m=as.numeric(m)
    n=as.numeric(n)
    x=as.numeric(x)
    res=data.frame(Term,Urn_Success=m,Urn_Failures=n,Trial_Success=x,Trial_effective=k,Urn_percentage_Success=signif(100*m/(m+n),3),Trial_percentage_Success=signif(100*x/k,3), Pvalue_over=phyper(x-1,m,n,k,lower.tail=FALSE),Pvalue_under=phyper(x,m,n,k,lower.tail=TRUE))

    res_over_under <-NULL
    index=which(res$Pvalue_over<Alpha)
    if(length(index)!=0)
    {
        res_over <- res[index,]
        res_over[,10] <- "overrepresented"
        colnames(res_over)[10] <- c("Decision")
        res_over_under <- res_over
    }
    
    index=which(res$Pvalue_under<Alpha)
    if(length(index)!=0)
    {
        res_under <- res[index,]
        res_under[,10] <- "underrepresented"
        colnames(res_under)[10] <- c("Decision")
        res_over_under <- rbind(res_over_under,res_under)
    }
    Results <- list("All_results" = res, "Over_Under_Results" = res_over_under)
    return(Results)
}

Enrichment <- function(Results_Directory, Project_Name, Title, Reference_Enrichment, Alpha_Enrichment = 0.01){

    if(is.null(Reference_Enrichment))
    {
        stop("\n Please put a file ", paste0(Project_Name,"_Enrichment.csv"), " in the Data directory and re-run the function Load_Data_Files before performing an enrichment test\n")
    }
    
    if(is.null(Title))
    {
        Contrast_Name= as.character(read.table(paste0(Results_Directory,"/",Project_Name,"/DiffAnalysis/",Project_Name,"_Contrasts_Interest_Matrix.txt"),header=TRUE,sep="\t")[,1])
        ## Summary results of all DEG lists
        Nb_Term=length(unique(Reference_Enrichment$Annotation))
        Resume=data.frame(Term=unique(Reference_Enrichment$Annotation),matrix(0,nrow=Nb_Term,ncol=length(Contrast_Name)+1))
        colnames(Resume)=c("Term",as.character(Contrast_Name),"Nb_enriched_clusters")
        l <- list()
        for (cc in 1:length(Contrast_Name))
        {
            l[[cc]]<-NULL
            ## Load Gene List file
            File_Id=paste0(Results_Directory,"/",Project_Name,"/DiffAnalysis/",Contrast_Name[cc],"/",Project_Name,"_",Contrast_Name[cc],"_Id_DEG.txt")
            if(file.exists(File_Id))
            {
                Gene_List <- read.csv2(File_Id,header=FALSE,sep="\t")
                colnames(Gene_List) <- c("Gene_ID")
                
                ## Perform enrichment analysis
                Results <- enrichment_analysis(Reference_Enrichment,Gene_List,Alpha_Enrichment)
                l[[cc]] <- Results$Over_Under_Results
                ## Save all results
                path=paste0(Results_Directory,"/",Project_Name,"/DiffAnalysis/",Contrast_Name[cc],"/",Project_Name,"_",Contrast_Name[cc])
                fileout=paste0(path,"_All_Enrichment_Results.txt")
                write.table(Results$All_results,fileout,row.names=FALSE,col.names=TRUE, sep="\t", quote = FALSE)
                ## Save significant results 
                if(!is.null(Results$Over_Under_Results))
                {
                    fileout=paste0(path,"_Significant_Enrichments.txt")
                    write.table(Results$Over_Under_Results,fileout,row.names=FALSE,col.names=TRUE, sep="\t", quote = FALSE)
                    index.col<-is.element(colnames(Resume),Contrast_Name[cc])
                    index.row <-is.element(Resume$Term,l[[cc]]$Term)
                    Resume[index.row,index.col] <- 1
                }
            }
        }
        Resume$Nb_enriched_clusters=rowSums(Resume[-c(1,ncol(Resume))])
        Resume=Resume[which(Resume$Nb_enriched_clusters!=0),]
        fileout=paste0(Results_Directory,"/",Project_Name,"/DiffAnalysis/",Project_Name,"_Summary_Enrichment.txt")
        write.table(Resume,fileout,row.names=FALSE,col.names=TRUE, sep="\t", quote = FALSE)
    }
    
    ## Title != NULL
    else
    {
        ## Load Cluster file (in Coexpression directory)
        path=paste0(Results_Directory,"/",Project_Name,"/Coexpression/",Title,"/",Project_Name,"_",Title)
        l <- list()
        Clusters <- read.csv2(paste0(path,"_AllClusters.txt"), header = TRUE, sep="\t",quote="")
        Clusters <- Clusters[,c("Gene_ID","Clusters")]

        ## Perform enrichment analysis
        Gene_List <-as.data.frame(Clusters[,1])
        colnames(Gene_List) <- c("Gene_ID")
        Results <- enrichment_analysis(Reference_Enrichment,Gene_List,Alpha_Enrichment)
        l[[1]] <- Results$Over_Under_Results
        ## Save results
        fileout=paste0(path,"_AllClusters_All_Enrichment_Results.txt")
        write.table(Results$All_results,file=fileout,row.names=FALSE,col.names=TRUE, sep="\t", quote = FALSE)
        ## Save significant results (over and under enriched)
        fileout=paste0(path,"_AllClusters_Significant_Enrichments.txt")
        write.table(Results$Over_Under_Results,fileout,row.names=FALSE,col.names=TRUE, sep="\t", quote = FALSE)
        ## List for the summary cluster file
        for (i in 1:max(Clusters$Clusters))
        {
            Gene_List <-as.data.frame(Clusters[which(Clusters$Clusters == i),1])
            colnames(Gene_List) <- c("Gene_ID")
            
            ## Perform enrichment analysis
            Results <- enrichment_analysis(Reference_Enrichment,Gene_List,Alpha_Enrichment)
            ## List for the summary cluster file
            l[[i+1]] <- Results$Over_Under_Results
            
            ## Save results
            fileout=paste0(path,"_Cluster_",i,"_All_Enrichment_Results.txt")
            write.table(Results$All_results,fileout,row.names=FALSE,col.names=TRUE, sep="\t", quote = FALSE)
            ## Save significant results (over and under enriched)
            if(!is.null(Results$Over_Under_Results))
            {
                fileout=paste0(path,"_Cluster_",i,"_Significant_Enrichments.txt")
                write.table(Results$Over_Under_Results,fileout,row.names=FALSE,col.names=TRUE, sep="\t", quote = FALSE)
            }
        }

        ## Summary results of all clusters
        Nb_Term=length(unique(Reference_Enrichment$Annotation))
        Resume=data.frame(Term=unique(Reference_Enrichment$Annotation),matrix(0,nrow=Nb_Term,ncol=max(Clusters$Clusters)+2))
        colnames(Resume)=c("Term",paste0("Cluster_",1:max(Clusters$Clusters)),"AllClusters","Nb_enriched_clusters")
        index.col=grep("AllClusters", colnames(Resume))
        index.row <-is.element(Resume$Term,l[[1]]$Term)
        Resume[index.row,index.col] <- 1
        for(j in 1:max(Clusters$Clusters))
        {
            if(!is.null(l[[j+1]]))
            {
                index.col=grep(paste0("Cluster_",j), colnames(Resume))
                index.row <-is.element(Resume$Term,l[[j]]$Term)
                Resume[index.row,index.col] <- 1
            }
        }
        Resume$Nb_enriched_clusters=rowSums(Resume[-c(1,(ncol(Resume)-1),ncol(Resume))])
        Resume=Resume[which(Resume$Nb_enriched_clusters!=0),]
        fileout=paste0(path,"_Summary_Cluster_Enrichment.txt")
        write.table(Resume,fileout,row.names=FALSE,col.names=TRUE, sep="\t", quote = FALSE)
    }
}


